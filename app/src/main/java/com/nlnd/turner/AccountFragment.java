package com.nlnd.turner;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AccountFragment extends Fragment
{
    TurnerUser user;
    public AccountFragment()
    {
        user = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        try
        {
            user = RestClient.getUserInfo(MainPageActivity.mainUser, getContext());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.account_fragment_layout, container, false);
        TextView name = (TextView) view.findViewById(R.id.account_page_name);
        TextView money = (TextView) view.findViewById(R.id.account_page_money);
        TextView phone = (TextView) view.findViewById(R.id.account_page_phone_number);
        ListView lv = (ListView) view.findViewById(R.id.account_page_transactions);
        TransactionsListView tlv = new TransactionsListView(getContext(),
                                    R.layout.transaction_list_view, user.getTransactions());
        lv.setAdapter(tlv);

        name.setText(user.getName());
        money.setText(String.valueOf(user.getMoney()) + "ریال");
        phone.setText(user.getPhoneNumber());


        return view;
    }
}
