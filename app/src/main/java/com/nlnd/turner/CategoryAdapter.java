package com.nlnd.turner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>
{
    private Activity context;
    private List<TurnerService> categories;
    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_view, parent, false);
        CategoryViewHolder viewHolder = new CategoryViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CategoryViewHolder holder, final int position)
    {
        holder.text.setText(categories.get(position).getName());
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                final Bitmap image = RestClient.getImage(categories.get(position).getId());
                context.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        holder.image.setImageBitmap(image);
                    }
                });
            }
        }).start();

        holder.image.setAdjustViewBounds(true);

        final Intent intent = new Intent(context, ShowService.class);
        intent.putExtra("service", position);
        intent.putExtra("category", categories.get(position).getCategory());

        holder.image.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return categories.size();
    }

    public CategoryAdapter(List<TurnerService> services, Context contex)
    {
        this.categories = services;
        this.context = (Activity) contex;
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder{

        protected ImageView image;
        protected TextView text;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image_id);
            text = itemView.findViewById(R.id.text_id);
        }
    }
}