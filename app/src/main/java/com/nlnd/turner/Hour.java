package com.nlnd.turner;

import android.os.Parcel;
import android.os.Parcelable;

import ir.huri.jcal.JalaliCalendar;

public class Hour implements Comparable<Hour>, Parcelable
{
    private String time;
    private String date;
    private boolean reserved;

    Hour(String time, String date)
    {
        this.time = time;
        this.date = date;
    }

    protected Hour(Parcel in)
    {
        time = in.readString();
        date = in.readString();
        reserved = in.readByte() != 0;
    }

    public static final Creator<Hour> CREATOR = new Creator<Hour>()
    {
        @Override
        public Hour createFromParcel(Parcel in)
        {
            return new Hour(in);
        }

        @Override
        public Hour[] newArray(int size)
        {
            return new Hour[size];
        }
    };

    public String getTime()
    {
        return time;
    }

    public void setTime(String time)
    {
        this.time = time;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    @Override
    public int compareTo(Hour arg0)
    {
        if (arg0.getDate().equals(this.getDate()))
            return Integer.parseInt(this.getTime()) - Integer.parseInt(arg0.getTime());
        else
            return Integer.parseInt(this.getDate()) - Integer.parseInt(arg0.getTime());
    }

    public boolean isReserved()
    {
        return reserved;
    }

    public void setReserved(boolean reserved)
    {
        this.reserved = reserved;
    }

    public String convertTime()
    {
        if (time.length() == 4)
            return time.substring(0, 2) + ":" + time.substring(2, 4);
        else
            return time.substring(0, 1) + ":" + time.substring(1, 3);
    }

    public String convertDate()
    {
        return convertDate(date);
    }

    public static String convertDate(String date)
    {
        if (date.length() == 8)
            return date.substring(0, 4) + "-" + date.substring(4,6)
                    + "-" + date.substring(6,8);
        else
            return date.substring(0,2) + "-" + date.substring(2, 4)
                    + "-" + date.substring(4,6);
    }

    public static String nextDate(String date)
    {
        return convert(date).getTomorrow().toString();
    }

    public static String prevDate(String date)
    {
        return convert(date).getYesterday().toString();
    }
    public static JalaliCalendar convert(String date)
    {
        return new JalaliCalendar(Integer.valueOf(date.split("-")[0]),
                Integer.valueOf(date.split("-")[1]),
                Integer.valueOf(date.split("-")[2]));
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(time);
        parcel.writeString(date);
        parcel.writeInt(reserved?1:0);
    }

}
