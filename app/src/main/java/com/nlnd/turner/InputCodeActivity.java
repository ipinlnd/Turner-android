package com.nlnd.turner;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.DataInputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class InputCodeActivity extends AppCompatActivity
{

    int code;
    String userCode = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_code);
        final EditText codeNumber = (EditText) findViewById(R.id.input_code_edit_text);
        Button enterButton = (Button) findViewById(R.id.input_code_button);
        final Intent intent = new Intent(this, MainPageActivity.class);
        if (savedInstanceState == null)
        {
            code = getIntent().getIntExtra("code", 0);
        }

        if (code == 0)
        {
            Toast.makeText(getApplicationContext(),
                    "مشکلی بوجود آمده", Toast.LENGTH_SHORT).show();
            finish();
        }

        enterButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String enteredCode = codeNumber.getText().toString();
                if (enteredCode.equals(String.valueOf(code)))
                {
                    new Thread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            try
                            {
                                userCode = RestClient.getUserCode();
                            } catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }).start();

                    while (userCode == null);

                    intent.putExtra("usercode", userCode);
                    startActivity(intent);;
                    return;
                }
                else
                {
                    Toast.makeText(getApplicationContext(),
                            "کد اشتباه است", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });
    }

    public void mainToolbarBackButton(View view)
    {
        finish();
    }
}
