package com.nlnd.turner;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.DataInputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

public class InputPhoneActivity extends AppCompatActivity
{

    public static int id = 0;
    int code = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //TODO: Start socket connection here

        setContentView(R.layout.activity_input_phone);

        final EditText phoneNumber = (EditText) findViewById(R.id.input_phone_edit_text);
        Button enterButton = (Button) findViewById(R.id.input_phone_button);

        final Intent intent = new Intent(this, InputCodeActivity.class);

        enterButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                final String number = phoneNumber.getText().toString();
                if (number == null || number.equals(""))
                {
                    Toast.makeText(getApplicationContext(),
                            "شماره تلفن نمیتواند خالی باشد", Toast.LENGTH_SHORT).show();
                    return;
                }
                MainPageActivity.phoneNumber = number;

                new Thread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        try
                        {
                            code = RestClient.addUser(number);
                        } catch (Exception e)
                        {
                            code = -1;
                            e.printStackTrace();
                        }
                    }
                }).start();

                while (code == 0);

                if (code == -1)
                {
                    Toast.makeText(getApplicationContext(),
                            "خطای ارتباط با سرور", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(getApplicationContext(),
                            "Sending code to " + number + " : " + code, Toast.LENGTH_SHORT).show();
                intent.putExtra("code", code);
                startActivity(intent);
                return;
            }
        });
    }
}
