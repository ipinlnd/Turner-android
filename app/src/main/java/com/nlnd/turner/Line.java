package com.nlnd.turner;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Line implements Parcelable
{
    String name;
    String price;
    String id;
    List<Hour> hours;

    Line(String name, String price)
    {
        this.name = name;
        this.price = price;
        hours = new ArrayList<>();
    }

    protected Line(Parcel in)
    {
        name = in.readString();
        price = in.readString();
        id = in.readString();
        hours = in.readArrayList(getClass().getClassLoader());
    }

    public static final Creator<Line> CREATOR = new Creator<Line>()
    {
        @Override
        public Line createFromParcel(Parcel in)
        {
            return new Line(in);
        }

        @Override
        public Line[] newArray(int size)
        {
            return new Line[size];
        }
    };

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getPrice()
    {
        return price;
    }

    public void setPrice(String price)
    {
        this.price = price;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(name);
        parcel.writeString(price);
        parcel.writeString(id);
        parcel.writeList(hours);
    }

    public void addToHours(Hour h)
    {
        this.hours.add(h);
        Collections.sort(this.hours);
    }
}

