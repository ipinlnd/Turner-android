package com.nlnd.turner;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainFragment extends Fragment
{
    public static List<TurnerCategory> categories;
    public static boolean loading = false;
    LinearLayout layout;

    public MainFragment()
    {
        categories = new ArrayList<>();
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        RestClient.getCategories();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.main_fragment_layout, container, false);
        layout = (LinearLayout) view.findViewById(R.id.mainPageLinearLayout);
        while (loading);

        for (int i = 0; i < categories.size(); i++)
        {
            TextView tv = new TextView(layout.getContext());
            tv.setText(categories.get(i).getName());
            tv.setTextSize(20);
            tv.setPadding(10,10,0,10);

            RecyclerView rv = new RecyclerView(layout.getContext());
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL, false);
            rv.setLayoutManager(layoutManager);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv.getContext(),
                    layoutManager.getOrientation());
            rv.addItemDecoration(dividerItemDecoration);
            RecyclerView.Adapter adapter = new CategoryAdapter(categories.get(i).getServices(), getActivity());
            rv.setAdapter(adapter);

            layout.addView(tv);
            layout.addView(rv);
        }
        return view;
    }
}
