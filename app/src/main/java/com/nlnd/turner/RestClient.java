package com.nlnd.turner;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.widget.FrameLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class RestClient
{
    private static final String SERVER_ADDRESS = "http://192.168.1.52:8080";
    private static boolean loading = false;

    private static HttpURLConnection makeReady(URL url)
    {
        HttpURLConnection urlConnection;
        try
        {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setReadTimeout(15*1000);
            urlConnection.connect();
            return urlConnection;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    private static String getResponse(HttpURLConnection urlConnection)
    {
        DataInputStream br;
        try
        {
            String response = "";
            int bytesRead;
            byte[] bytes = new byte[1000];
            br = new DataInputStream(urlConnection.getInputStream());
            while ((bytesRead = br.read(bytes)) > 0)
                response += new String(bytes, 0, bytesRead, "UTF-8");
            return response;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static int addUser(String phone)
    {
        int number = -1;
        URL url = null;
        try
        {
            url = new URL( SERVER_ADDRESS + "/users/add?phone=" + phone);
            String response = "";
            HttpURLConnection urlConnection = makeReady(url);
            response = getResponse(urlConnection);

            if (response == null)
                return number;

            number = Integer.parseInt(response);
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        return number;
    }

    public static String getUserCode()
    {
        String id;

        URL url = null;
        try
        {
            url = new URL( SERVER_ADDRESS + "/users/phone/" + MainPageActivity.phoneNumber);
            String response = "";
            HttpURLConnection urlConnection = makeReady(url);
            response = getResponse(urlConnection);
            id = response;
            return id;
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public static boolean validateId(String id)
    {
        boolean ans;

        URL url = null;
        try
        {
            url = new URL( SERVER_ADDRESS + "/users/validate/" + id);
            HttpURLConnection urlConnection = makeReady(url);
            String response = getResponse(urlConnection);
            ans = Boolean.parseBoolean(response);
            return ans;
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    public static Bitmap getImage(String id)
    {
        URL url = null;
        try
        {
            url = new URL(SERVER_ADDRESS + "/category/image/" + id);
            final HttpURLConnection urlConnection = makeReady(url);
            DataInputStream br = new DataInputStream(urlConnection.getInputStream());
            byte[] bytes = new byte[1000];

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
            int bytesRead;

            while ((bytesRead = br.read(bytes)) > 0)
                outputStream.write(bytes, 0, bytesRead);

            byte[] all = outputStream.toByteArray();
            return BitmapFactory.decodeByteArray(all, 0, all.length);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static void getCategories()
    {
        MainFragment.loading = true;
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                String response = "";
                try
                {
                    URL url = new URL( SERVER_ADDRESS + "/category");

                    final HttpURLConnection urlConnection = makeReady(url);
                    DataInputStream br = new DataInputStream(urlConnection.getInputStream());
                    response = getResponse(urlConnection);

                    JSONArray category = new JSONArray(response);
                    for (int i =0;i <category.length(); i++)
                    {
                        JSONObject categoryObject = category.getJSONObject(i);
                        TurnerCategory newCat = new TurnerCategory(categoryObject.getString("name"));
                        newCat.setId(categoryObject.getString("id"));
                        JSONArray services = categoryObject.getJSONArray("services");
                        for (int j=0;j < services.length(); j++)
                        {
                            JSONObject serviceObject = services.getJSONObject(j);
                            TurnerService newService = new TurnerService(serviceObject.getString("name"),
                                    serviceObject.getString("id"));
                            newService.setAddress(serviceObject.getString("address"));
                            newService.setCategory(i);
                            JSONArray lines = serviceObject.getJSONArray("lines");
                            for (int k =0; k< lines.length(); k++)
                            {
                                JSONObject lineObject = lines.getJSONObject(k);
                                Line line = new Line(lineObject.getString("name"),
                                        lineObject.getString("price"));
                                line.setId(lineObject.getString("id"));
                                JSONArray lineHours = lineObject.getJSONArray("hours");
                                for (int x =0; x<lineHours.length();x++)
                                {
                                    JSONObject hour = lineHours.getJSONObject(x);
                                    Hour h = new Hour(hour.getString("time"), hour.getString("date"));
                                    h.setReserved(hour.getBoolean("reserved"));
                                    line.addToHours(h);
                                }
                                newService.addToLines(line);
                            }
                            newCat.addToServices(newService);
                        }
                        MainFragment.categories.add(newCat);
                    }
                    MainFragment.loading = false;
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static TurnerUser getUserInfo(final String id, Context context)
    {
        final TurnerUser user = new TurnerUser();
        loading = true;

        ProgressDialog progressDialog = new ProgressDialog(context,
                ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("در حال بارگیری داده ها...");
        progressDialog.getWindow().setLayout(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                byte[] bytes = new byte[1000];
                int bytesRead;
                URL url;
                try
                {
                    url = new URL( SERVER_ADDRESS + "/users/" + id);
                    final HttpURLConnection urlConnection = makeReady(url);
                    DataInputStream br = new DataInputStream(urlConnection.getInputStream());
                    String response = getResponse(urlConnection);

                    JSONObject object = new JSONObject(response);
                    user.setName(object.getString("name"));
                    user.setId(object.getString("id"));
                    user.setPhoneNumber(object.getString("phoneNumber"));
                    user.setMoney(object.getLong("money"));
                    JSONArray trans = object.getJSONArray("transactions");

                    for (int i =0; i < trans.length(); i++)
                    {
                        JSONObject tran = trans.getJSONObject(i);
                        Transaction t = new Transaction(tran.getString("user"),
                                tran.getString("service"),
                                tran.getString("line"),
                                tran.getString("hour"));
                        t.setId(tran.getString("id"));
                        user.addToTransactions(t);
                    }
                    loading = false;
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        }).start();

        while (loading);

        progressDialog.dismiss();
        return  user;
    }

    public static String getServiceName(final String service)
    {
        try
        {
            URL url = new URL( SERVER_ADDRESS + "/category/service/" + service + "/name");
            HttpURLConnection urlConnection = makeReady(url);
            String response = getResponse(urlConnection);

            return response;
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    public static String getServiceType(String line)
    {
        try
        {
            URL url = new URL( SERVER_ADDRESS + "/category/line/" + line + "/name");
            HttpURLConnection urlConnection = makeReady(url);
            String response = getResponse(urlConnection);

            return response;
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    public static String reserve(int serv, String id, int position)
    {
        try
        {
            URL url = new URL(SERVER_ADDRESS + "/category/reserve/" +
                                MainFragment.categories.get(ShowService.cat).getServices().get(serv).id
                                + "/" + id + "/" + String.valueOf(position) + "/" + MainPageActivity.mainUser);
            HttpURLConnection urlConnection = makeReady(url);
            String response = getResponse(urlConnection);
            return response;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    public static String cancelReservation(String transactionId)
    {
        try
        {
            URL url = new URL(SERVER_ADDRESS + "/category/unreserve/" + transactionId + "/" +
                                MainPageActivity.mainUser);

            HttpURLConnection urlConnection = makeReady(url);
            String response = getResponse(urlConnection);
            return response;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
