package com.nlnd.turner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class ServiceDetailsListAdapter extends ArrayAdapter<Line>
{
    Activity context;
    List<Line> lines;

    public ServiceDetailsListAdapter(@NonNull Context context, int resource, @NonNull List<Line> objects)
    {
        super(context, resource, objects);
        this.context = (Activity) context;
        this.lines = objects;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        LayoutInflater inflater = context.getLayoutInflater();
        final View rowView = inflater.inflate(R.layout.service_details_list_view, null, true);
        TextView name = (TextView) rowView.findViewById(R.id.service_details_line_name);
        TextView price = (TextView) rowView.findViewById(R.id.service_details_line_price);
        Button showHours = (Button) rowView.findViewById(R.id.service_details_hours_button);

        name.setText(lines.get(position).name);
        price.setText(lines.get(position).price);
        showHours.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(context, ShowHours.class);
                intent.putExtra("line", lines.get(position));
                context.startActivity(intent);
            }
        });

        return rowView;
    }
}
