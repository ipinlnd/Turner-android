package com.nlnd.turner;

import android.app.Activity;
import android.icu.text.DateFormat;
import android.icu.util.Calendar;
import android.icu.util.ULocale;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ListView;
import android.widget.TextView;

import java.util.GregorianCalendar;

import ir.huri.jcal.JalaliCalendar;

public class ShowHours extends Activity
{
    public static Line line;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.show_hours_layout);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width * 0.8), (int)(height * 0.5));

        if (savedInstanceState == null)
        {
            line = getIntent().getParcelableExtra("line");
        }
        if (line == null)
            finish();
        setView();
    }

    boolean selected = false;
    float x;
    boolean next = false;
    boolean prev = false;
    String date = "";

    private void setView()
    {
        JalaliCalendar today = new JalaliCalendar();
        date = String.valueOf(today.getYear()) + "-" + String.format("%02d", today.getMonth())
                                            + "-" + String.format("%02d", today.getDay());

        ShowHoursListAdapter shla = new ShowHoursListAdapter(this,
                                                        R.layout.show_hours_layout, line.hours);
        final ListView lv = (ListView) findViewById(R.id.show_hours_list_view);
        lv.setAdapter(shla);

        final TextView tv = (TextView) findViewById(R.id.show_hours_date_viewer);
        tv.setText(date);

        tv.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onClick()
            {
                animate();
            }

            @Override
            public void onSwipeUp()
            {
                animate();
            }

            @Override
            public void onSwipeLeft()
            {
                date = Hour.prevDate(date);
                prevDayAnimation(tv, date);
            }

            @Override
            public void onSwipeRight()
            {
                date = Hour.nextDate(date);
                nextDayAnimation(tv, date);
            }
        });
    }

    private void nextDayAnimation(final TextView tv, final String date)
    {
        TranslateAnimation ta = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_PARENT, 1.0f,
                Animation.RELATIVE_TO_PARENT, 0f,
                Animation.RELATIVE_TO_PARENT, 0f);
        ta.setDuration(900);
        final TranslateAnimation ta2 = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_PARENT, 0f,
                Animation.RELATIVE_TO_PARENT, 0f);
        ta2.setDuration(900);
        tv.startAnimation(ta);

        ta.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {

            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                tv.clearAnimation();
                tv.startAnimation(ta2);
                tv.setText(date);
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {

            }
        });
    }

    private void prevDayAnimation(final TextView tv, final String date)
    {
        TranslateAnimation ta = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0f,
                Animation.RELATIVE_TO_PARENT, 0f);
        ta.setDuration(900);
        final TranslateAnimation ta2 = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 1.0f,
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0f,
                Animation.RELATIVE_TO_PARENT, 0f);
        ta2.setDuration(900);
        tv.startAnimation(ta);

        ta.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {

            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                tv.clearAnimation();
                tv.startAnimation(ta2);
                tv.setText(date);
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {

            }
        });
    }

    void animate()
    {
        if (!selected)
        {
            final ListView lv = (ListView) findViewById(R.id.show_hours_list_view);
            final TextView tv = (TextView) findViewById(R.id.show_hours_date_viewer);
            TranslateAnimation ta = new TranslateAnimation(
                    Animation.RELATIVE_TO_PARENT, 0,
                    Animation.RELATIVE_TO_PARENT, 0.2f,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, -0.3f);

            ScaleAnimation sa = new ScaleAnimation(1.0f, 0.4f, 1.0f, 0.4f);

            AnimationSet set = new AnimationSet(true);
            set.addAnimation(sa);
            set.addAnimation(ta);
            set.setFillAfter(true);
            set.setDuration(900);

            tv.clearAnimation();
            tv.startAnimation(set);

            AlphaAnimation aa = new AlphaAnimation(0.0f, 1.0f);
            aa.setFillAfter(true);
            aa.setDuration(900);
            lv.clearAnimation();
            lv.startAnimation(aa);
            selected = true;
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }
}
