package com.nlnd.turner;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

class ShowHoursListAdapter extends ArrayAdapter<Hour>
{
    List<Hour>  hours;
    Activity context;

    public ShowHoursListAdapter(@NonNull Context context, int resource, @NonNull List<Hour> objects)
    {
        super(context, resource, objects);
        this.context = (Activity) context;
        this.hours = objects;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        LayoutInflater inflater = context.getLayoutInflater();
        final View rowView = inflater.inflate(R.layout.show_hours_list_view, null, true);

        TextView time = (TextView) rowView.findViewById(R.id.show_hours_time_text);
        Button reserve = (Button) rowView.findViewById(R.id.show_hours_reserve);

        time.setText(hours.get(position).convertTime());
        reserve.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                new Thread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        RestClient.reserve(ShowService.serv,
                                            ShowHours.line.getId(), position);
                        context.runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                Toast.makeText(context, "رزرو شما انجام شد", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }).start();
            }
        });

        return rowView;
    }
}
