package com.nlnd.turner;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ShowService extends AppCompatActivity
{
    TurnerService service = null;
    public static int cat, serv;
    ImageView image;
    Bitmap bitmap;
    boolean gotImage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_service);

        Toolbar toolbar = (Toolbar) findViewById(R.id.show_service_toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null)
        {
            cat = getIntent().getIntExtra("category", -1);
            serv = getIntent().getIntExtra("service", -1);
        }

        if (cat == -1 || serv == -1)
        {
            Toast.makeText(getApplicationContext(),
                    "مشکلی بوجود آمده", Toast.LENGTH_SHORT).show();
            finish();
        }

        service = MainFragment.categories.get(cat).getServices().get(serv);
        image = (ImageView) findViewById(R.id.show_service_image_view);
        getImage();

        ListView lv = (ListView) findViewById(R.id.show_service_list_view);
        ServiceDetailsListAdapter sdla = new ServiceDetailsListAdapter(this,
                    R.layout.service_details_list_view, service.lines);
        lv.setAdapter(sdla);

    }

    public void getImage()
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    bitmap = RestClient.getImage(service.getId());
                    gotImage = true;
                } catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        }).start();

        while(!gotImage);

        image.setImageBitmap(bitmap);
    }
}
