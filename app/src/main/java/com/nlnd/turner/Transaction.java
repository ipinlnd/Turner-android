package com.nlnd.turner;

import java.util.UUID;

public class Transaction implements Comparable<Transaction>
{
    private String	user;
    private String service;
    private String line;
    private String hour;
    private String id;

    public Transaction(String user, String service, String line, String hour)
    {
        this.user = user;
        this.service = service;
        this.line = line;
        this.hour = hour;
        id = UUID.randomUUID().toString().replaceAll("-", "");
    }

    public String getUser()
    {
        return user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }
    public String getService()
    {
        return service;
    }
    public void setService(String service)
    {
        this.service = service;
    }
    public String getLine()
    {
        return line;
    }
    public void setLine(String line)
    {
        this.line = line;
    }
    public String getHour()
    {
        String t = hour.split(",")[0];
        String d = hour.split(",")[1];

        Hour h = new Hour(t, d);

        return (h.convertDate() + " " + h.convertTime());
    }
    public void setHour(String hour)
    {
        this.hour = hour;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    @Override
    public int compareTo(Transaction o)
    {
        return this.getHour().compareTo(o.getHour());
    }
}
