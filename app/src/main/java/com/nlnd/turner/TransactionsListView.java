package com.nlnd.turner;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class TransactionsListView extends ArrayAdapter<Transaction>
{
    Activity context;
    List<Transaction> transactions;
    public TransactionsListView(@NonNull Context context, int resource, @NonNull List<Transaction> objects)
    {
        super(context, resource, objects);
        this.context = (Activity) context;
        transactions = objects;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        LayoutInflater inflater = context.getLayoutInflater();
        final View rowView = inflater.inflate(R.layout.transaction_list_view, null, true);

        final TextView serviceName = rowView.findViewById(R.id.transaction_list_view_service_name);
        final TextView serviceType = rowView.findViewById(R.id.transaction_list_view_service_type);
        final TextView time = rowView.findViewById(R.id.transaction_list_view_time);
        final ImageView icon = rowView.findViewById(R.id.transaction_list_view_image_view);
        final Button cancel = rowView.findViewById(R.id.transaction_list_view_cancel_butt);

        serviceName.setText("");
        serviceType.setText("");
        time.setText("");

        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
               final String sn = RestClient.getServiceName(transactions.get(position).getService());
               final String st = RestClient.getServiceType(transactions.get(position).getLine());
               final String t = transactions.get(position).getHour();
               final Bitmap image = RestClient.getImage(transactions.get(position).getService());
               context.runOnUiThread(new Runnable()
               {
                   @Override
                   public void run()
                   {
                       serviceName.setText(sn);
                       serviceType.setText(st);
                       time.setText(t);
                       icon.setImageBitmap(image);
                   }
               });
            }
        }).start();

        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                cancelReservation(transactions.get(position).getId());
                                transactions.remove(position);
                                notifyDataSetChanged();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setMessage("آیا مطمئنید که میخواهید لغو کنید").setPositiveButton("بله",
                        dialogClickListener).setNegativeButton("خیر ",
                        dialogClickListener).show();
            }
        });

        return rowView;
    }

    private void cancelReservation(final String id)
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                RestClient.cancelReservation(id);
            }
        }).start();
    }
}
