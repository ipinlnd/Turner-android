package com.nlnd.turner;

import java.util.ArrayList;
import java.util.List;

class TurnerCategory
{
    private String name;
    private String id;
    private List<TurnerService> services;

    TurnerCategory(String name)
    {
        services = new ArrayList<>();
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void addToServices(TurnerService newService)
    {
        services.add(newService);
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public List<TurnerService> getServices()
    {
        return services;
    }

    public void setServices(List<TurnerService> services)
    {
        this.services = services;
    }
}
