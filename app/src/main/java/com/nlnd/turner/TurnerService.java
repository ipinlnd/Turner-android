package com.nlnd.turner;

import java.util.ArrayList;
import java.util.List;

class TurnerService
{
    int image;
    String name;
    String id;
    String address;
    int category;
    List<Line>  lines;

    public int getCategory()
    {
        return category;
    }

    public void setCategory(int category)
    {
        this.category = category;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    TurnerService(String name, String id)
    {
        this.name = name;
        lines = new ArrayList<>();
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public int getImage()
    {
        return image;
    }

    public void setImage(int image)
    {
        this.image = image;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void addToLines(Line line)
    {
        lines.add(line);
    }
}
