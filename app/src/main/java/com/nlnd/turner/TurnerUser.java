package com.nlnd.turner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class TurnerUser
{
    private String name;
    private String id;
    private String phoneNumber;
    private long money;
    private List<Transaction> transactions;

    public TurnerUser(String name, String id)
    {
        transactions = new ArrayList<>();
        this.id = id;
        this.name = name;
    }

    public TurnerUser()
    {
        transactions = new ArrayList<>();
    }

    public String getName()
    {
        return name;
    }

    public String getId()
    {
        return id;
    }

    public long getMoney()
    {
        return money;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public void setMoney(long money)
    {
        this.money = money;
    }

    public List<Transaction> getTransactions()
    {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions)
    {
        this.transactions = transactions;
        Collections.sort(this.transactions);
    }

    public void addToTransactions(Transaction t)
    {
        this.transactions.add(t);
        Collections.sort(this.transactions);
    }

}